package de.mdstv.raspi.pictrl.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.mdstv.raspi.pictrl.PiCtrl;

/**
 * Describes a client connection
 * @author Morph
 * @since 1.0
 */
public class Client {
	public static final long MAX_HEARTHBEAT = 10; // In seconds
	
	private static final Logger logger     = PiCtrl.LOGGER;
	private Socket              connection = null;
	
	private long lastHearthbeat = System.currentTimeMillis();
	
	private final ArrayList<IntercationListener> interactionListeners =
			new ArrayList<>();
	
	private boolean disconnectRequested = false;
	
	/**
	 * Create new Client from Socket
	 * @param connection
	 */
	public Client(Socket connection) {
		// Check for null
		if (connection == null) {
			throw new NullPointerException("Could not create Client with Socket == null");
		}
		
		// Assign connection
		this.connection = connection;
		
		// Startup client loop
		this.initLoop();
	}
	
	// Prepare and start client loop
	private void initLoop() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				clientLoop();
			}
		}).start();
	}
	
	/**
	 * The client loop
	 */
	private void clientLoop() {
		while (getConnection().isConnected() && !getConnection().isClosed()) {
			// Input stream handling
			BufferedReader br = getInput();
			
			// Read from input
			while (getConnection().isConnected()) {
				// Check, if connection is closed
				if (getConnection().isClosed()) {
					break;
				}
				
				String line = null;
				try {
					line = br.readLine();
				} catch (SocketException e) {
					// Client is disconnected - break client loop.
					break;
				} catch (IOException e) {
					logger.log(Level.SEVERE, "Could not read data from " + this.toString(), e);
				}
				
				// Check if line is null (=input closed)
				// If there is input, the client is still alive!
				if (line == null) {
					break;
				} else {
					this.lastHearthbeat = System.currentTimeMillis();
				}
				
				for (IntercationListener listener : interactionListeners) {
					listener.onInput(this, line);
				}
			}
			
			try {
				Thread.sleep(25);
			} catch (InterruptedException e) {
				logger.log(Level.SEVERE, "Interruption while client loop for " + this.toString(), e);
			}
		}
	}
	
	/**
	 * Register a {@link IntercationListener} to this client
	 * You can listen for 
	 * @param listener
	 */
	public void registerInteractionListener(IntercationListener listener) {
		// Check for null
		if (listener == null) {
			throw new NullPointerException("Trying to add null as ClientInteractionListener");
		}
		
		// Add listener
		this.interactionListeners.add(listener);
		
		// Debug info
		logger.info("Added InteractionListener to " + this.toString());
	}
	
    /**
     * Returns all Interaction
     * @return 
     */
	public List<IntercationListener> getInteractionListeners() {
		return interactionListeners;
	}
	
	/**
	 * Returns the {@link Socket} connection of this {@link Client}
	 * @return
	 */
	public Socket getConnection() {
		return connection;
	}

	/**
	 * Gets the Client IP as {@link String}
	 * @return Client IP as String
	 */
	public String getIP() {
		return getConnection().getInetAddress().toString();
	}
	
	/**
	 * Gets the input stream for this client
	 * @return
	 */
	public BufferedReader getInput() {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(getConnection().getInputStream()));
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Could not get input stream for " + this.toString(), e);
		}
		
		return reader;
	}
	
	/**
	 * Get the output for this client
	 * @return
	 */
	public PrintStream getOutput() {
		PrintStream writer = null;
		try {
			writer = new PrintStream(getConnection().getOutputStream());
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Could not get output stream for " + this.toString(), e);
		}
		
		return writer;
	}

	/**
	 * Send raw data to client
	 * @param string Data to send
	 */
	public void sendRawData(final String data) {
		getOutput().println(data);
	}
	
	/**
	 * Check if client is alive
	 * If disconnect was requested, this method will return false
	 * @return <code>true</code> if hearthbeat was still sent, <code>false</code>
	 * if hearthbeat was not sent for too long while (>MAX_HEARTHBEAT) or if
	 * disconnect was requested.
	 */
	public boolean isClientAlive() {
		if (isDisconnectRequested()) {
			return false;
		} else {
			return (System.currentTimeMillis() - lastHearthbeat) < (MAX_HEARTHBEAT * 1000);
		}
	}
	
	/**
	 * On disconnect request the client will disconnected by {@link ClientDispatcher}
	 * in next dispatcherLoop run.
	 */
	public void requestDisconnect() {
		// Inform client
		sendRawData("Disconnect Requested!");
		
		// Set disconnect request flag
		this.disconnectRequested = true;
	}
	
	/**
	 * Returns disconnect request flag
	 * @return
	 */
	public boolean isDisconnectRequested() {
		return this.disconnectRequested;
	}
	
	@Override
	public String toString() {
		return "Client[" + getIP() + "]";
	}
}
