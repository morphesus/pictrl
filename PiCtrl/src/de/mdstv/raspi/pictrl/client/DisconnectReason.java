package de.mdstv.raspi.pictrl.client;

public enum DisconnectReason {
	HEATHBEAT_TIMEOUT("Sorry, timeout. Please send more hearthbeats to keep the connection alive."),
	DISCONNECT_REQUESTED("Disconnect was requested by Client");
	
	private final String reason;
	private DisconnectReason(final String reason) {
		this.reason = reason;
	}
	
	public String getReason() {
		return this.reason;
	}
}
