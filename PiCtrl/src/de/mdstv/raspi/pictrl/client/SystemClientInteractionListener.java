package de.mdstv.raspi.pictrl.client;

import de.mdstv.raspi.pictrl.PiCtrl;
import java.util.Arrays;

import de.mdstv.raspi.pictrl.client.command.CommandDispatcher;

public class SystemClientInteractionListener implements IntercationListener {
	private CommandDispatcher cmdDispatcher = new CommandDispatcher();
	
	@Override
	public void onInput(Client client, String input) {
		System.out.println(client.toString() + " says: " + input);
		
		// Split input by space (First is command, following are params)
		String[] cmdSlices = input.split(" ");
		String[] cmdArgs   = Arrays.copyOfRange(cmdSlices, 1, cmdSlices.length);
		
		// Switch input for system commands or send it to CommandDispatcher
		switch (cmdSlices[0]) {
		// The 'quit' command disconnects the Client
		case "quit":
			client.requestDisconnect();
			break;
            
        // Sends the uptime to client
        case "uptime":
            client.sendRawData("Current uptime is " + PiCtrl.getUptime() + "ms");
            break;
           
        // server base command (needs args)
        case "server":
            // Check if args available
            if (cmdArgs.length == 0) {
                client.sendRawData("No parameters given");
                break;
            }
            
            switch (cmdArgs[0]) {
                case "stop":
                    // TODO Check Client permission
                    // client.hasPermission("server.stop");
                    PiCtrl.endpoint.killEndpoint();
                    
                    break;
                default:
                    client.sendRawData("Unknown subcommand - aborting!");
                    
            }
            
            break;
		
		// The hb command is simply a hearthbeat and should not be processed
		case "hb":
			break;
		
		// This is a client command, send it to CommandDispatcher
		default:
			// Execute Command with splitted command data
			cmdDispatcher.executeCommand(client, cmdSlices[0], cmdArgs);
		}
	}

	@Override
	public void onLogin(Client client) {
		
	}

	@Override
	public void onLogout(Client client) {
		
	}

	@Override
	public void onConnected(Client client) {
		System.out.println("Client Connected: " + client.getIP());
	}

	@Override
	public void onDisconnected(Client client, DisconnectReason reason) {
		System.out.println("Client Disconnected: " + client.getIP() + " (" + reason.getReason() + ")");
	}
}
