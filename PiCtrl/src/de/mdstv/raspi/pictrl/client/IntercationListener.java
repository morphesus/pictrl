package de.mdstv.raspi.pictrl.client;

public interface IntercationListener {
	// IO
	public void onInput(Client client, String input);
	
	// Login/Logout
	public void onLogin(Client client);
	public void onLogout(Client client);
	
	// Connection
	public void onConnected(Client client);
	public void onDisconnected(Client client, DisconnectReason reason);
}
