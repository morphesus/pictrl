package de.mdstv.raspi.pictrl.client;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.mdstv.raspi.pictrl.PiCtrl;

/**
 * Manages clients and handles in/output
 * @author Morph
 * @since 1.0
 *
 */
public class ClientDispatcher {
	private static final Logger     logger  = PiCtrl.LOGGER;
	private final ArrayList<Client> clients = new ArrayList<>();
	
	private ServerSocket server;
	
	/**
	 * Initialize the Dispatcher
	 * @param server
	 */
	public ClientDispatcher(ServerSocket server) {
		this.server = server;
		
		// Init dispatcher loop
		this.initDispatcherLoop();
	}
	
	/**
	 * Initializes the dispatcher loop
	 */
	private void initDispatcherLoop() {
		logger.info("Initializing dispatcher loop");
		
		// Create and start Thread for loop
		new Thread(new Runnable() {
			@Override
			public void run() {
				dispatcherLoop();
			}
		}).start();
	}

    /**
     * This message will be sent to all connected clients
     * @param msg 
     */
    public void broadcast(String msg) {
        for (Client client : this.clients) {
            client.sendRawData(msg);
        }
    }
    
    /**
     * Disconnects all clients and sends the given reason to all clients before
     * disconnect them.
     * @param reason 
     */
    public void disconnectAll(String reason) {
        // Send shutdown message to all connected clients
        broadcast(reason);
        
        // Disconnect all clients
        for (Client client : this.clients) {
            try {
                client.getConnection().close();
            } catch (IOException ex) {
                logger.log(Level.SEVERE, "Error while disconnecting Client", ex);
            }
        }
    }

	/**
	 * The dispatcher loop manages the connected clients and does a cleanUp,
	 * if client has disconnected
	 */
	private void dispatcherLoop() {
		while (!server.isClosed()) {
			// Check for disconnected clients
			clientGarbageCollector();
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.log(Level.SEVERE, "Interruption in ClientDispatcher loop", e);
			}
		}
	}
	
	/**
	 * Registers a connected client to Dispatcher
	 * @param clientSocket
	 */
	public void registerClient(Client client) {
		// Check for null
		if (client == null) {
			logger.severe("Trying to add null as client");
			return;
		}
		
		// Check for connected state
		if (!client.getConnection().isConnected()) {
			logger.severe("Trying to add not connected client");
			return;
		}
		
		// Add clients to list
		this.clients.add(client);
		
		// Get registered InteractionListeners
		List<IntercationListener> listeners =
				client.getInteractionListeners();
		
		// Fire interaction listener
		if (listeners.size() > 0) {
			for (IntercationListener iListener : listeners) {
				iListener.onConnected(client);
			}
		}
		
		// Get Client IP
		String ip = client.getConnection().getInetAddress().toString();
		
		// When client is added output little Debug info
		logger.log(Level.INFO, "Client[{0}] was registered", ip);
	}

	/**
	 * Removes all disconnected clients from list
	 */
	public void clientGarbageCollector() {
		int closedConnections = 0;
		for (int i = 0; i < this.clients.size(); i++) {
			// Get Client and Socket connection
			Client c = this.clients.get(i);
			Socket s = c.getConnection();
			
			// Check if client is disconnected
			if (!c.isClientAlive()) {
				// Remove from list
				clients.remove(i);
				
				// If not closed, try to close connection before drop client
				if (!s.isClosed()) {
					try {
						s.close();
					} catch (IOException e) {
						logger.log(Level.SEVERE, "Could not close client connection", e);
					}
				}
				// Fire disconnected event
				for (IntercationListener listener : c.getInteractionListeners()) {
					if (c.isDisconnectRequested()) {
						listener.onDisconnected(c, DisconnectReason.DISCONNECT_REQUESTED);
					} else {
						listener.onDisconnected(c, DisconnectReason.HEATHBEAT_TIMEOUT);
					}
				}
				
				// Increase disconnect counter
				closedConnections++;
			}
		}
		
		// Debug info after cleanup, if some clients are kicked out
		if (closedConnections > 0) {
			logger.log(Level.INFO, "ClientGarbageCollector closed {0} connections.", closedConnections);
		}
	}
}
