package de.mdstv.raspi.pictrl.client.command;

import java.util.ArrayList;
import java.util.logging.Logger;

import de.mdstv.raspi.pictrl.PiCtrl;
import de.mdstv.raspi.pictrl.client.Client;

public class CommandDispatcher {
	private Logger logger = PiCtrl.LOGGER;
	private ArrayList<ClientCommand> commands = new ArrayList<ClientCommand>();
	
	/**
	 * Registers a new client command
	 * @param command
	 */
	public void registerCommand(ClientCommand command) {
		// Check for null command
		if (command == null) {
			throw new NullPointerException("Command cannot be null");
		}
		
		// Add command to list
		this.commands.add(command);
		
		// System info
		logger.info("Added new command '" + command.getName() + "'");
	}
	
	/**
	 * Checks, if the given {@link ClientCommand} is registered
	 * @param name The name of the {@link ClientCommand}
	 * @return <code>true</code> if registered, <code>false</code> if not
	 */
	public boolean isCommandRegistered(final String name) {
		return this.commands.contains(name);
	}
	
	/**
	 * Returns the {@link ClientCommand} for given name. If the command is not
	 * existing <code>null</code> will returned.
	 * @param name Name of the {@link ClientCommand}
	 * @return {@link ClientCommand} if found, <code>null</code> if this command
	 * isn't existing
	 */
	public ClientCommand getCommandByName(final String name) {
		for (ClientCommand cmd : commands) {
			if (cmd.getName().equalsIgnoreCase(name)) {
				return cmd;
			}
		}
		
		return null;
	}
	
	/**
	 * Executes given {@link ClientCommand}
	 * @param name {@link ClientCommand} to execute
	 * @param args Parameters to use
	 * @return The response from executed command
	 */
	public String executeCommand(final Client client, final ClientCommand command, final String[] args) {
		return this.executeCommand(client, command.getName(), args);
	}
	
	/**
	 * Executes the {@link ClientCommand} with given name and params.
	 * @param name Name of the {@link ClientCommand}
	 * @param args Parameters to use
	 * @return The response from executed command
	 */
	public String executeCommand(final Client client, final String name, final String[] args) {
		// Check if client is null (Client is needed for ClientCommands)
		if (client == null) {
			logger.severe("Could not execute ClientCommand without Client");
			return null;
		}
		
		// Get the command
		ClientCommand cmd = getCommandByName(name);
		
		// Check, if cmd is null (not existing)
		if (cmd == null) {
			// Send info to client
			client.sendRawData("Command not found");
			
			logger.severe("Unknown command: " + name);
			return null;
		}
		
		// Execute command and get response
		String commandResponse = cmd.execute(args);
		
		// Send response to client
		client.sendRawData(commandResponse);
		
		// Return response for internal processing
		return commandResponse;
	}
}
