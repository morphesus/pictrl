package de.mdstv.raspi.pictrl.client.command;

public abstract class ClientCommand {
    /**
     * Returns the name of this ClientCommand
     * @return 
     */
	public abstract String getName();
    
    /**
     * Executes this command with given args
     * @param args Arguments to append
     * @return Maybe the command output or <code>null</code> if there is nothing
     */
	public abstract String execute(String[] args);
}
