package de.mdstv.raspi.pictrl.endpoint;

import java.util.logging.Logger;

import de.mdstv.raspi.pictrl.PiCtrl;
import de.mdstv.raspi.pictrl.client.ClientDispatcher;
import de.mdstv.raspi.pictrl.client.SystemClientInteractionListener;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.logging.Level;

/**
 * The {@link AbstractEndpoint} is the base for any serving service for PiCtrl
 * It manages base functionality like accepting client connections
 * @author Morph
 * @since 1.0
 */
public abstract class AbstractEndpoint {
	private EndpointType     type   = EndpointType.UNKNOWN;
	private volatile boolean alive  = false;

	protected Logger         logger = PiCtrl.LOGGER;
        
	protected int            port   = 8010;
	protected ServerSocket   serverSocket;
	
	protected ClientDispatcher                clientDispatcher;
	protected SystemClientInteractionListener listener;
    
    protected Thread serverLoopThread;
	
	/**
	 * Initalizes this endpoint
	 * @param type {@link EndpointType}
	 */
	protected final void init(EndpointType type) {
		// Check type
		if (type == EndpointType.UNKNOWN) {
			logger.log(Level.SEVERE, "Could not init endpoint with type {0}", type.name());
			return;
		}
		
		// Debug log
		logger.log(Level.INFO, "Initalizing endpoint with type ''{0}''", type.name());
		
		// Set the type
		this.type = type;
		
		// Init the loop
		this.initLoop();
	}
	
    /**
     * Initializes the server loop
     */
	private void initLoop() {
        // Debug log
        logger.info("Initalizing endpoint loop");

        // While the endpoint is alive, do the loop
        this.alive = true;
        this.serverLoopThread = new Thread(new Runnable(){
            @Override
            public void run() {
                while (alive) {
                    serverLoop();
                }

                logger.info("Endpoint has stopped!");
            }
        });
        
        // Start the server loop
        this.serverLoopThread.start();
	}
	
	/**
	 * Get type of this endpoint
	 * @return {@link EndpointType}
	 */
	public EndpointType getEndpointType() {
		return this.type;
	}
	
	/**
	 * Check if the endpoint is still alive
	 * @return <code>true</code> if the endpoint is still alive,
	 * <code>false</code> otherwise
	 */
	public boolean isAlive() {
		return this.alive;
	}
	
	/**
	 * Kills the endpoint in next loop execution
	 * Current loop will <b>not</b> interrupted!
     * 
     * Connected client will informed for shutdown.
	 */
	public void killEndpoint() {
		this.alive = false;
        
        // Inform and disconnect clients
        this.clientDispatcher.disconnectAll("Server is shutting down!");
        
        // Close server
        try {
            this.serverSocket.close();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Error while closing Server", ex);
        }
	}
    
	/**
	 * Here the magic is done!
	 */
	protected abstract void serverLoop();
}
