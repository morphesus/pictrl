package de.mdstv.raspi.pictrl.endpoint;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.logging.Level;

import de.mdstv.raspi.pictrl.client.Client;
import de.mdstv.raspi.pictrl.client.ClientDispatcher;
import de.mdstv.raspi.pictrl.client.SystemClientInteractionListener;
import java.net.BindException;

/**
 * This is the default server
 * All base magic is done here
 * @author Morph
 * @since 1.0
 *
 */
public class DefaultServer extends AbstractEndpoint {
	/**
	 * Creates and initializes the server endpoint
	 */
	public DefaultServer() {
        this.initServer();
        this.init(EndpointType.DEFAULT_SERVER);
	}
	
    /**
     * Initializes the server
     */
    private void initServer() {
		// Debug info
		logger.log(Level.INFO, "Initalizing server to listen on port {0}", port);
		
		// Startup server
		try {
			serverSocket = new ServerSocket(port);
		} catch (BindException e) {
            logger.log(Level.SEVERE, "Could not bind Server at port " + port, e);
            System.exit(1);
        } catch (Exception e) {
			logger.log(Level.SEVERE, "Could not create ServerSocket", e);
		}
		
		// Initialize ClientDispatcher
		logger.info("Initializing ClientDispatcher");
		clientDispatcher = new ClientDispatcher(serverSocket);
		
		// Initialize global listener
		listener = new SystemClientInteractionListener();
	}

	@Override
	protected void serverLoop() {
		// Debug info
		logger.log(Level.INFO, "Waiting for connection on port {0}", serverSocket.getLocalPort());
		
		// Client Connection
		Client client = null;
		try {
			// Wait for connection
			client = new Client(serverSocket.accept());
			client.registerInteractionListener(listener);
		} catch (IOException e) {
            // Only log error, if server is not shutting down
            if (isAlive()) {
                logger.log(Level.SEVERE, "Client connection failed", e);
            }
		}
		
		// Register Client to Dispatcher, if server is alive
        if (isAlive()) {
            clientDispatcher.registerClient(client);
        }
	}
}
