package de.mdstv.raspi.pictrl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import de.mdstv.raspi.pictrl.endpoint.AbstractEndpoint;
import de.mdstv.raspi.pictrl.endpoint.DefaultServer;

public class PiCtrl {
    /**
     * Remember the startup timestamp
     */
    public static final long startUp = System.currentTimeMillis();
    /**
     * Global endpoint to use - do not overwrite!
     */
    public static AbstractEndpoint endpoint;
    /**
     * Global PiCtrl Logger
     */
    public static final Logger LOGGER = Logger.getLogger("pictrl");

    /*
     * Command line arguments START
     */
    @Argument
    private List<String> arguments = new ArrayList<>();
    @Option(name = "-type", usage = "-type <server_type>")
    private String serverType = "default";
    @Option(name = "-config", usage = "-config <path_to_config>")
    private String configFile;
    @Option(name = "-whitelist", usage = "-whitelist <path_to_whitelist>")
    private String whitelistFile = "whitelist.txt";

    /*
     * Command line arguments END
     */
    public static void main(String[] args) {
        new PiCtrl().doMain(args);
    }

    /**
     * Boot the application
     *
     * @param args
     */
    private void doMain(String[] args) {
        // Init cmd line parser
        final CmdLineParser parser = new CmdLineParser(this);

        // Set max usage text width
        parser.setUsageWidth(80);

        // Parse arguments
        // Exit with error status code on failure
        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            LOGGER.log(Level.SEVERE, "Could not parse command line arguments", e);
            System.exit(1);
        }

        // Check server type
        // If type is not given, exit instantly with error code
        if (serverType == null) {
            LOGGER.severe("You have to give the server type such as -type default");
            System.exit(1);
        }

        // PiCtrl instance to startup server
        final PiCtrl inst = new PiCtrl();

        // Check serving type
        switch (serverType) {
            case "default":
                inst.startServer();
                break;
            default:
                LOGGER.log(Level.SEVERE, "Unknown serving type given ''{0}''! Please start application with type ''default''", serverType);
                System.exit(1);
        }
    }
    
    /**
     * Starts up the server endpoint
     */
    private void startServer() {
        endpoint = new DefaultServer();
    }

    /**
     * Returns the current uptime
     *
     * @return
     */
    public static long getUptime() {
        return System.currentTimeMillis() - startUp;
    }
}
